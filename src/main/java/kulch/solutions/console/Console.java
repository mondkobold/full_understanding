package kulch.solutions.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
    public static void main(String[] args) {
        //init
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        CommandInterpreter interpreter = new CommandInterpreter();
        String command = "";

        //Print Greeting
        System.out.println("Welcome to the Kulch Solutions Learning Console!");
        interpreter.listCommandsToStdOut();
        boolean quit = false;
        while (!quit) {
            System.out.print("Please enter your command:");

            // Read input and delegate command
            command = "";
            try {
                command = reader.readLine();
            } catch (IOException e) {
                //just keep going
            }
            int retVal = interpreter.performCommand(command.trim());
            if (retVal == 1) {
                quit = true;
            } else if(retVal == -1) {
                System.out.println("--- action not Supported");
            }
        }
        System.out.println("See you later Aligator!");
    }
}

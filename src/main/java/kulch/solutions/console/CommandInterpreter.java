package kulch.solutions.console;

public class CommandInterpreter {

    private static String LIST_COMMANDS = "help";
    private static String START_GUI = "start x";
    private static String CLOSE_APPLICATION = "quit";
    private static String RANDOM_TEXT = "random";


    /**
     * This method will match user Commands and trigger the choosen Action.
     *
     * @param command the entered command
     * @return 0 if the input matches any command
     *         1 if the application should be closed
     *        -1 if no match was found
     */
    public int performCommand(String command) {

        if(CLOSE_APPLICATION.equalsIgnoreCase(command)) {
            return 1;
        }
        if(LIST_COMMANDS.equalsIgnoreCase(command)) {
            listCommandsToStdOut();
            return 0;
        }

        return -1;
    }

    void listCommandsToStdOut() {
        StringBuilder commandslist = new StringBuilder("");
        commandslist.append("The following commands are supported:\n");
        commandslist.append("\t");
        commandslist.append(LIST_COMMANDS);
        commandslist.append("\t");
        commandslist.append(": lists all possible actions\n");
        commandslist.append("\t");
        commandslist.append(START_GUI);
        commandslist.append("\t");
        commandslist.append(": starts the application gui\n");
        commandslist.append("\t");
        commandslist.append(CLOSE_APPLICATION);
        commandslist.append("\t");
        commandslist.append(": closes the application\n");
        commandslist.append("\t");
        commandslist.append(RANDOM_TEXT);
        commandslist.append("\t");
        commandslist.append(": produces some random text\n");
        System.out.print(commandslist);
    }
}

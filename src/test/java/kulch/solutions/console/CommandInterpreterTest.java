package kulch.solutions.console;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandInterpreterTest {

    @Test
    public void performCommand() {
        CommandInterpreter itpr = new CommandInterpreter();
        assertEquals(0, itpr.performCommand("help"));
        assertEquals(1, itpr.performCommand("quit"));
        assertEquals(-1, itpr.performCommand("mule"));
    }
}